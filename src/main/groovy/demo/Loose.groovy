package demo

class Loose {
    def static equals(Map one, Map two) {
        if (!two.keySet().containsAll(one.keySet())) return false
        one.every { it.value == two[it.key] }
    }
}

