package demo

class FluentWrapper {
    def delegate

    FluentWrapper(wrapped) {
        delegate = wrapped
    }

    def methodMissing(String name, args) {
        def method = delegate.getClass().declaredMethods.find { it.name == name }
        if (method) {
            method.invoke(delegate, args)
            return this
        } else throw new MissingMethodException(name, delegate, args)
    }
}
