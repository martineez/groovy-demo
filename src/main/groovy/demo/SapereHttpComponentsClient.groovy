package demo

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.http.HttpHost
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.BasicResponseHandler
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.protocol.HTTP

class SapereHttpComponentsClient {
    def target
    def DefaultHttpClient client
    def String authToken
    final LOGIN_PATH = '/vendavo/rest/login'

    SapereHttpComponentsClient(url, userName, password) {
        target = new HttpHost('localhost', 8080, 'http');

        client = new DefaultHttpClient();
        authToken = getToken(userName, password)
    }

    String getToken(userName, password) {
        def post = new HttpPost(LOGIN_PATH)
        post.setHeader(HTTP.CONTENT_TYPE, 'application/json')
        post.setEntity(new StringEntity(JsonOutput.toJson([username: userName, password: password])))
        def response = client.execute(target, post, new BasicResponseHandler())
        Map result = new JsonSlurper().parseText(response) as Map<String, ?>

        result.'auth-token'
    }

    def getTimePeriods(String request) {
        def uri = new URIBuilder('/vendavo/api/pa/business-calendar/time-periods')
                .addParameter('request', request)
                .build()

        def get = new HttpGet(uri)
        get.setHeader(HTTP.CONTENT_TYPE, Version.V1)
        get.setHeader('Accept', Version.V1)
        get.addHeader('Auth-Token', authToken)

        client.execute(target, get, new BasicResponseHandler())
    }
}
