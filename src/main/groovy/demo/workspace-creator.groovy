package demo

final USERNAME = 'vendavosystem'
final PASSWORD = 'vendavosystem'

//final URL = 'http://localhost:8080'
//final URL = 'http://features-sapere.vmcz.vendavo.com:7500'
//final URL = 'http://sapere-automation.vmcz.vendavo.com:7500'
final URL = 'http://sapere-functional.vmcz.vendavo.com:7500'

def client = new SapereRESTClient(URL, USERNAME, PASSWORD)

(1..60).each {
    client.createWorkspace(String.format("John Majx %02d", it))
}