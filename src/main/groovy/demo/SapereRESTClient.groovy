package demo

import groovy.json.JsonSlurper
import groovyx.net.http.ContentType
import groovyx.net.http.RESTClient

class SapereRESTClient {
    final LOGIN_PATH = '/vendavo/rest/login'
    def client
    def authToken

    SapereRESTClient(url, userName, password) {
        client = new RESTClient(url)
        authToken = getToken(userName, password)
    }

    String getToken(userName, password) {
        def resp = client.post(
                path: LOGIN_PATH,
                body: [username: userName, password: password],
                requestContentType: 'application/json')

        resp.data.get('auth-token')
    }

    List getAllDashboards() {
        def resp = client.get(
                path: '/vendavo/api/pa/analysis/explorer-view',
                headers: ['Auth-Token'  : authToken,
                          'Content-Type': Version.V1,
                          'Accept'      : Version.V1
                ])

        new JsonSlurper().parseText(resp.data.text).content
    }

    void deleteDashboard(id) {
        def resp = client.delete(
                path: "/vendavo/api/pa/analysis/explorer-view/$id",
                headers: ['Auth-Token'  : authToken,
                          'Content-Type': Version.V1,
                          'Accept'      : Version.V1
                ])

        println "Dashboard $id deleted."
    }

    List getAllWorkspaces() {
        def resp = client.get(
                path: '/vendavo/api/pa/analysis/explorer',
                headers: ['Auth-Token'  : authToken,
                          'Content-Type': Version.V1,
                          'Accept'      : Version.V1
                ])

        def ids = new JsonSlurper().parseText(resp.data.text).content
        //ids.each { println it.id }
        ids
    }

    void deleteWorkspace(id) {
        def resp = client.delete(
                path: "/vendavo/api/pa/analysis/explorer/$id",
                headers: ['Auth-Token'  : authToken,
                          'Content-Type': Version.V1,
                          'Accept'      : Version.V1
                ])

        println "Workspace $id deleted."
    }

    void createWorkspace(name) {
        def resp = client.post(
            path: "/vendavo/api/pa/analysis/explorer",
            headers: ['Auth-Token'  : authToken,
                      'Content-Type': Version.V1,
                      'Accept'      : Version.V1
            ],
            requestContentType: ContentType.JSON,
            body: [
                'name': name,
                'description': '',
                'tags': [],
                'accessRight': 'own',
                'sharing': [
                    'mode': 'private',
                    'accessRights': [
                        'view': [],
                        'edit': [],
                        'own': []
                    ]
                ],
                'config': [
                    'activeTab': -1,
                    'filterSettings': [
                        'base': [
                            'convertUsing': 'variableDate',
                            'exchangeRateDate': '01/01/2015',
                            'dataSource': 'Pricemart_Transactions',
                            'targetCurrency': 'USD',
                            'targetUnit': 'Each',
                            'lifecycle': 'Transactions'
                        ],
                        'comparison': [
                            'convertUsing': 'variableDate',
                            'exchangeRateDate': '01/01/2015',
                            'lifecycle': 'Transactions'
                        ],
                        'hasComparison': false
                    ],
                    'filters': [
                        'base': [
                            'before': [],
                            'after': [
                                'groupBy': null,
                                'filters': []
                            ]
                        ],
                        'comparison': [
                            'before': [],
                            'after': [
                                'groupBy': null,
                                'filters': []
                            ]
                        ]
                    ],
                    'components': []
                ]
            ]
        )

        println "Workspace $name created."
    }
}
