package demo

final USERNAME = 'vendavosystem'
final PASSWORD = 'vendavosystem'

final URL = 'http://localhost:8080'
//final URL = 'http://features-sapere.vmcz.vendavo.com:7500'
//final URL = 'http://sapere-automation.vmcz.vendavo.com:7500'
//final URL = 'http://sapere-functional.vmcz.vendavo.com:7500'

def client = new SapereRESTClient(URL, USERNAME, PASSWORD)

client.getAllDashboards().each { client.deleteDashboard(it.id) }
client.getAllWorkspaces().each { client.deleteWorkspace(it.id) }