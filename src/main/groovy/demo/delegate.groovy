package demo

//def say = {println m}
def say = {
    def m = 'hello'
    println m
}

say.delegate = [m:2]
say()
