package demo

class Person {
    int age
    String name

    public void setAge( int age ) { this.age = age }
    public void setName( String name ) { this.name = name }
    public String toString() { "$name $age" }
}