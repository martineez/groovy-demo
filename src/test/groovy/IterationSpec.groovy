import spock.lang.Specification

class IterationSpec extends Specification {
    def fibList = [1, 1, 2, 3, 5]

    //fibList.each { println it }  // prints all of the numbers in the list

    def any() {
        expect:
        fibList.any { it == 3 }
    }

    def every() {
        expect:
        fibList.every { it > 0 }
    }

    def collect() {
        expect:
        fibList.collect { it - 1 } == [0, 0, 1, 2, 4]
    }

    def collect2() {
        given:
        def initialFruits = ["Orange", "Lemon"]
        def fruits = ["Banana", "Apple", "Grape", "Pear"]
        when:
        def totalFruits = fruits.collect(initialFruits, { it.toUpperCase() })
        then:
        totalFruits == ["Orange", "Lemon", "BANANA", "APPLE", "GRAPE", "PEAR"]
        initialFruits == ["Orange", "Lemon", "BANANA", "APPLE", "GRAPE", "PEAR"]
    }

    def distinct() {
        given:
        def fruits = ["Banana", "Apple", "Grape", "Pear", "Banana"]
        when:
        def distinctFruits = fruits.collect(new HashSet(), { it })
        then:
        distinctFruits.toArray() == ["Apple", "Pear", "Grape", "Banana"]
    }

    def findAll() {
        expect:
        fibList.findAll { it > 1 && it < 5 } == [2, 3]
    }

    def find() {
        expect:
        fibList.find { it > 1 } == 2
    }

    def inject() {
        expect:
        fibList.inject("fib: ") { str, val -> str << val }.toString() == "fib: 11235"
    }

    def inject2() {
        given:
        def wordList = ["Groovy", "dynamic", "Grails", "Gradle", "scripting"]
        def string = "This is an example blog talking about Groovy and Gradle."

        // at least one element from wordList is in the string
        expect:
        wordList.inject(false) { acc, value -> acc || string.contains(value) }
    }

    def collectNested() {
        def list = [1, 2, 3, [5, 6]]
        expect:
        println(list.collectNested() { it })
    }

    def each() {
        def map = [a: 1, b: 2, c: 3]
        def actual = map.each { k, v -> [k, v + 1] }
        expect:
        println actual
        println map
    }

    def collectEntries() {
        def map = [a: 1, b: 2, c: 3]
        def actual1 = map.collectEntries { k,v -> [k, v + 1] }
        def actual2 = map.collectEntries { [it.key, it.value + 1] }
        expect:
        map == [a: 1, b: 2, c: 3]
        actual1 == [a: 2, b: 3, c: 4]
        actual2 == [a: 2, b: 3, c: 4]
    }
}
