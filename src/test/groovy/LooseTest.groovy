import demo.Loose

use(Loose) {
//    assert [a: 1, b: 2] == [a: 1, b: 2, c: 3]
    assert [a: 1, b: 2, c: 3] == [a: 1, b: 2]
}