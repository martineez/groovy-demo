import spock.lang.Specification

import java.math.RoundingMode


class RoundMapSpec extends Specification {

    def private BigDecimal round(BigDecimal value, int precision) {
        value.setScale(precision, RoundingMode.FLOOR)
    }

    def private List round(List value, int precision) {
        value.collectNested {
            switch (it) {
                case BigDecimal.class:
                case Map.class:
                    return round(it, precision)
                default:
                    return it
            }
        }
    }

    def private Map round(Map value, int precision) {
        value.collectEntries {
            def result
            switch (it.value) {
                case BigDecimal.class:
                case List.class:
                case Map.class:
                    result = round(it.value, precision)
                    break
                default:
                    result = it.value
            }
            [it.key, result]
        }
    }

    def "compare two BigDecimals"() {
        expect:
        round(1.7788341045, 2) == 1.77
    }

    def "compare two lists"() {
        def actual = round([1, 'abc', true, 1.7788341045], 2)

        expect:
        actual == [1, 'abc', true, 1.77]
    }

    def "compare two nested lists"() {
        def actual = round([1, 'abc', true, 1.7788341045, [1, 'abc', true, 1.7788341045]], 2)

        expect:
        actual == [1, 'abc', true, 1.77, [1, 'abc', true, 1.77]]
    }

    def "compare two lists with map"() {
        def actual = round([1, 'abc', true, 1.7788341045, [value: 2.7788341045]], 2)

        expect:
        actual == [1, 'abc', true, 1.77, [value: 2.77]]
    }

    def "Compare recursive map with rounding"() {
        when:
        def actual = round([
            result: [
                content: [
                    base: [
                        series: [
                            [
                                name   : 'ABC',
                                content: [
                                    18451689.7788341045,
                                    34588013.2685367465,
                                    8362787.4839726472,
                                    5853983.3291805983
                                ]
                            ], [
                                name   : 'DEF',
                                content: [
                                    18451689.7788341045,
                                    34588013.2685367465,
                                    8362787.4839726472,
                                    5853983.3291805983
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],2)

        def expected = round([
            result: [
                content: [
                    base: [
                        series: [
                            [
                                name   : 'ABC',
                                content: [
                                    18451689.7788341045,
                                    34588013.2685367465,
                                    8362787.4839726472,
                                    5853983.3291805983
                                ]
                            ], [
                                name   : 'DEF',
                                content: [
                                    18451689.77889999,
                                    34588013.26859999,
                                    8362787.483979999,
                                    5853983.329180999
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],2)

        then:
//        round(expected, 2) == round(actual, 2)
        expected == actual
    }

    def "around"() {
//        when:
        def doubleValue = 12.5456d
        def floatValue = 987.654f

        expect:
        assert 12.546d == doubleValue.round(3)
        assert 13 == doubleValue.round()

        assert 12 == doubleValue.trunc()
        assert 12.54d == doubleValue.trunc(2)


        assert 987.65f == floatValue.round(2)
        assert 988 == floatValue.round()
        assert 987.6f == floatValue.trunc(1)
        assert 987 == floatValue.trunc()
    }

    def switchTest() {
        def actual = [
                result: [
                        content: [
                                base: [
                                        series: [
                                                [
                                                        name   : 'ABC',
                                                        content: [
                                                                18451689.7788341045,
                                                                34588013.2685367465,
                                                                8362787.4839726472,
                                                                5853983.3291805983
                                                        ]
                                                ], [
                                                        name   : 'DEF',
                                                        content: [
                                                                18451689.7788341045,
                                                                34588013.2685367465,
                                                                8362787.4839726472,
                                                                5853983.3291805983
                                                        ]
                                                ]
                                        ]
                                ]
                        ]
                ]
        ]

        expect:
        actual.collect {
            switch (it) {
                case Map.Entry.class:
                    println 'entry'
                    break
                case BigDecimal.class:
                case List.class:
                    println 'list'
                    break
                case Map.class:
                    println 'map'
                    break
                default:
                    it
            }
        }

    }

    def collectTest() {
        def a = [name: 'a', year:1900, enabled:true]
        def b = a.collect {
            return it
        }
//        def c = b.collect {
//            it + '1'
//        }
        expect:
        println b
//        println c
    }
}