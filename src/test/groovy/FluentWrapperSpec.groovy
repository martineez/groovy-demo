import demo.FluentWrapper
import demo.Person
import spock.lang.Specification

class FluentWrapperSpec extends Specification {
    def "delegate should work"() {
        when:
        def wrappedPerson = new FluentWrapper(new Person())
        Person person = wrappedPerson.setAge(85).setName('tim').delegate

        then:
        person.name == 'tim'
        person.age == 85
    }
}