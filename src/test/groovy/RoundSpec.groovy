import spock.lang.Specification

class RoundSpec extends Specification {
    def doubleTest() {
        given:
        def doubleValue = 12.5456d

        expect:
        doubleValue.round(3) == 12.546d
        doubleValue.round() == 13
        doubleValue.trunc() == 12
        doubleValue.trunc(2) == 12.54d
    }

    def floatTest() {
        given:
        def floatValue = 987.654f

        expect:
        floatValue.round(2) == 987.65f
        floatValue.round() == 988
        floatValue.trunc(1) == 987.6f
        floatValue.trunc() == 987
    }
}