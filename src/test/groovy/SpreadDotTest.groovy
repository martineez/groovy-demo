String NAME = "name";
String LABEL = "label";
String TYPE = "type";
String CURRENCY = "currency";
String THOUSANDS_SEPARATOR = "thousandsSeparator";
String NUMERIC_SUFFIX = "numericSuffix";
String DECIMAL_SEPARATOR = "decimalSeparator";
String DECIMAL_PRECISION = "decimalPrecision";

def map = [
        percent: [
                (NAME)               : 'percent',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : '% of Total',
                (NUMERIC_SUFFIX)     : '%',
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'percentage'
        ],
        total  : [
                (NAME)               : 'total',
                (CURRENCY)           : 'USD',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : 'Invoice Price',
                (NUMERIC_SUFFIX)     : null,
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'money'
        ],
        from   : [
                (NAME)               : 'from',
                (CURRENCY)           : 'USD',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : 'From COGS',
                (NUMERIC_SUFFIX)     : null,
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'money'
        ],
        to     : [
                (NAME)               : 'to',
                (CURRENCY)           : 'USD',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : 'To COGS',
                (NUMERIC_SUFFIX)     : null,
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'money'
        ]
]

def list = [
        [
                (NAME)               : 'percent',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : '% of Total',
                (NUMERIC_SUFFIX)     : '%',
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'percentage'
        ],
        [
                (NAME)               : 'total',
                (CURRENCY)           : 'USD',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : 'Invoice Price',
                (NUMERIC_SUFFIX)     : null,
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'money'
        ],
        [
                (NAME)               : 'from',
                (CURRENCY)           : 'USD',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : 'From COGS',
                (NUMERIC_SUFFIX)     : null,
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'money'
        ],
        [
                (NAME)               : 'to',
                (CURRENCY)           : 'USD',
                (DECIMAL_PRECISION)  : 2,
                (DECIMAL_SEPARATOR)  : '.',
                (LABEL)              : 'To COGS',
                (NUMERIC_SUFFIX)     : null,
                (THOUSANDS_SEPARATOR): ',',
                (TYPE)               : 'money'
        ]
]

map.collect {println(it*.name)}
//println((1..10).collect { it * 2 })
//println((1..10)*.multiply(2))
assert map.decimalPrecision == 2
//assert list*.decimalPrecision == 2
map.collect()
list.collect()