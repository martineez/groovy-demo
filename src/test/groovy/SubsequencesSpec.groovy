import spock.lang.Specification

class SubsequencesSpec extends Specification {
    def 'subsequences from number'() {
        given:
        def number = "246" as List

        when:
        def seq = number.subsequences()

        then:
        [2, 246, 46, 24, 4, 26, 6] == seq.collect { it.join() as int }
    }
}